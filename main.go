package main

import (
	"fmt"
	"os"
	"net"

	"github.com/knusbaum/go9p"
	"github.com/hanwen/go-fuse/fuse"
	"github.com/hanwen/go-fuse/fuse/nodefs"
	"github.com/hanwen/go-fuse/fuse/pathfs"
)

type NinepFS struct {
	pathfs.FileSystem
}

var maxSize uint32

func (me *NinepFS) GetAttr(name string, context *fuse.Context) (*fuse.Attr, fuse.Status) {
	switch name {
	case "file.txt":
		return &fuse.Attr{
			Mode: fuse.S_IFREG | 0644, Size: uint64(len(name)),
		}, fuse.OK
	case "":
		return &fuse.Attr{
			Mode: fuse.S_IFDIR | 0755,
		}, fuse.OK
	}
	return nil, fuse.ENOENT
}

func (me *NinepFS) OpenDir(name string, context *fuse.Context) (c []fuse.DirEntry, code fuse.Status) {
	if name == "" {
		c = []fuse.DirEntry{{Name: "file.txt", Mode: fuse.S_IFREG}}
		return c, fuse.OK
	}
	return nil, fuse.ENOENT
}

func (me *NinepFS) Open(name string, flags uint32, context *fuse.Context) (file nodefs.File, code fuse.Status) {
	if name != "file.txt" {
		return nil, fuse.ENOENT
	}
	if flags&fuse.O_ANYWRITE != 0 {
		return nil, fuse.EPERM
	}
	return nodefs.NewDataFile([]byte(name)), fuse.OK
}

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:9999")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't connect to 127.0.0.1:9999: %s\n", err)
		return
	}

	vers := go9p.TRVersion{go9p.FCall{go9p.Tversion, 0}, 8192, "9P2000"}
	conn.Write(vers.Compose())
	ifcall, err := go9p.ParseCall(conn)
	fmt.Fprintf(os.Stderr, "Got fcall return!\n")

	if ifcall.GetFCall().Ctype != go9p.Rversion {
		fmt.Fprintf(os.Stderr, "It wasn't an RVersion! How Weird! (%s)\n", ifcall.GetFCall().Ctype)
	}
	rvers := ifcall.(*go9p.TRVersion)
	maxSize = rvers.Msize

	fmt.Fprintf(os.Stderr, "Got msize: %d\n", maxSize);

	ninepfs := pathfs.NewPathNodeFs(&NinepFS{ pathfs.NewDefaultFileSystem()}, nil)
	server, _, err := nodefs.MountRoot("/mnt/9p", ninepfs.Root(), nil)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to mount filesystem: %s\n", err)
		return
	}
	server.Serve()
}
